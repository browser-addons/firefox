This addon adds a livestreamer context menu item when browsing twitch.tv.
It expects livestreamer to be configured with appropriate options separately, otherwise, it can't run.
