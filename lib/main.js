const {Cc, Ci} = require("chrome");
var cm = require("sdk/context-menu");
var { MatchPattern } = require("sdk/util/match-pattern");

var twitch_channel_pattern = new MatchPattern(/.*\.twitch\.tv\/\w+$/);
var twitch_clip_pattern =    new MatchPattern(/.*\.twitch\.tv\/\w+\/c\/\d+$/);
var twitch_dir_pattern =     new MatchPattern(/.*\.twitch\.tv\/directory\/.*/);

var url = "";

cm.Item({
  label: "Play with livestreamer",
  context: cm.PredicateContext(function(context) {
    console.log(context);
    if (twitch_channel_pattern.test(context.documentURL) ||
        twitch_clip_pattern.test(context.documentURL)) {
      url = context.documentURL;
      return true;
    } 

    return false;
  }),
  contentScript: 'self.on("click", function (node, data) {' +
                 '  self.postMessage();' +
                 '});',
  onMessage: function () {
    runLivestreamer(url);
  }
});

var runLivestreamer = function(url) {
  // create an nsIFile for the executable
  var file = Cc["@mozilla.org/file/local;1"]
               .createInstance(Ci.nsIFile);
  file.initWithPath("/bin/termite");

  // create an nsIProcess
  var process = Cc["@mozilla.org/process/util;1"]
                  .createInstance(Ci.nsIProcess);
  process.init(file);

  // Run the process.
  // If first param is true, calling thread will be blocked until
  // called process terminates.
  // Second and third params are used to pass command-line arguments
  // to the process.
  var args = ["-e", "livestreamer", url];
  process.runwAsync(args, args.length);
}
